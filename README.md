# Getting started with MQTT Viewer

The MQTT Viewer is a web application that displays the traffic that go through a <a href="https://gitlab.com/mqtt-broker-sniffer/sniffer">MQTT Broker Recorder</a>.

# Deployment

## Manual installation

The first requirement is to install the version `v16.15.0` of NodeJS with `npm 8.5.5`.

You can then clone the gitlab repository, install the npm dependencies, build the app,
and start it.

```
git clone https://gitlab.com/mqtt-broker-sniffer/client.git
cd ./client
npm install --force
npm install -g serve
npm run build
serve -s build -l <port>
```

The `<port>` is the TCP port the viewer will use.

### Plugin installation

You can install plugins for the viewer through a npm command.

```
npm run install_plugin <url>
```

where the `<url>` is the URL of the zipped plugin.

## Docker

You can run the lastest deployed Docker image with the following commands:

```
docker pull martindetienne/mqtt-viewer
docker run -p 80:4030 --name mqtt-viewer -d martindetienne/mqtt-viewer
```

This image will fetch the messages from http://localhost:4040.

### Build

The viewer can be built in the Docker environment. First, clone the gitlab repository, and build in docker compose.

```
git clone https://gitlab.com/mqtt-broker-sniffer/client.git
cd ./client
sudo docker -compose up -d
```

## Configuration

The viewer can be configured thanks to the `src/config/production.js` file. If the parameter is not specified, its default value from `src/config/default.js` is used.

| Parameter           | Default       | Description                                                                               |
| ------------------- | ------------- | ----------------------------------------------------------------------------------------- |
| brokerAddress       | `"localhost"` | The IP of the broker from which the MQTT messages are recorded.                           |
| brokerAPIPort       | `4040`        | The port used by the broker for the API.                                                  |
| maxFetchPublication | `100`         | The number of MQTT messages that are fetched at the beginning of a session in the viewer. |

# Documentation

The documentation is present <a href="./doc">here</a>. Learn more how to make plugins <a href="./doc/plugins">here</a>. You can find exemples of plugins in this <a href="https://gitlab.com/mqtt-broker-sniffer/plugins">repository</a>.
