import React from "react";
import { Route } from "react-router-dom";
import { Routes } from "react-router-dom";
import { BrowserRouter } from "react-router-dom";
import Layout from "./layout/Layout";
import useStyle from "./layout/Style";
import Home from "./Home";
import Published from "./viewer/Published";

export default function App() {
  useStyle();

  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <Layout>
              <Home />
            </Layout>
          }
        />
        <Route
          path="/published/:brokerID"
          element={
            <Layout>
              <Published />
            </Layout>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}
