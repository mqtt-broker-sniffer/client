import { Grid } from "@mui/material";
import React from "react";
import BrokerState from "../../model/BrokerState";
import Topics from "./Topics";
import PacketFlow from "./PacketFlow";
import { Typography } from "@mui/material";
import Clients from "./Clients";
import PropTypes from "prop-types";

/**
 * Display the broker view
 *
 * @param {{broker: BrokerState}} props the broker state to display
 */
const BrokerView = ({ broker }) => {
  return (
    <div id="brokerView">
      <PacketFlow broker={broker} />

      <Grid container spacing={10} justifyContent="space-between">
        <Grid item xs={4}>
          <Typography variant="h5" color="primary" align="center">
            Senders
          </Typography>
          <Clients broker={broker} sender={true} />
        </Grid>
        <Grid item xs={4}>
          <Typography variant="h5" color="primary" align="center">
            Topics
          </Typography>
          <Topics broker={broker} />
        </Grid>
        <Grid item xs={4}>
          <Typography variant="h5" color="primary" align="center">
            Receivers
          </Typography>
          <Clients broker={broker} sender={false} />
        </Grid>
      </Grid>
    </div>
  );
};

BrokerView.propTypes = {
  /**
   * The broker state to display
   */
  broker: PropTypes.instanceOf(BrokerState).isRequired,
};

export default BrokerView;
