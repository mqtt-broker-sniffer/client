import { Popover } from "@mui/material";
import React, { useState } from "react";
import Xarrow from "react-xarrows";
import {
  ReceiverFilter,
  SenderFilter,
  TopicFilter,
} from "../../model/filter/BasicFilters";
import { AndFilter } from "../../model/filter/OperatorFilters";
import MessageList from "../packets/MessageList";
import PropTypes from "prop-types";
import BrokerState from "../../model/BrokerState";
import { useStyles } from "./styles";

/**
 * Display an arrow
 *
 * @param {{start: string, end: string, onClick: () => void, anim: boolean}} props the
 *  start and the end document id of the arrow, the callback on click, and if the animation have
 *  to be activated
 */
const FlowArrow = ({ start, end, onClick, anim = false }) => {
  // The width of the arrow
  const [width, setWidth] = useState(5);

  return (
    <Xarrow
      start={start}
      end={end}
      startAnchor="right"
      endAnchor="left"
      strokeWidth={width}
      passProps={{
        onClick,
        cursor: "pointer",
        onMouseEnter: () => setWidth(8),
        onMouseLeave: () => setWidth(5),
      }}
      dashness={anim ? { animation: 2 } : false}
      curveness={0.5}
    />
  );
};

/**
 * Display a flow of messages
 *
 * @param {{client: string | {client: string, subscribed: boolean}, topic, string, publication: boolean, broker: BrokerState}} props the id of the client (indicates if the client is subscribed if not publication) and the topic associated with the flow and if this is a publication or a forward and the broker state
 */
const Flow = ({ client, topic, publication, broker }) => {
  const classes = useStyles();

  // If the flow has been selected by the user
  const [open, setOpen] = useState(false);

  return (
    <div>
      {publication ? (
        <FlowArrow
          start={`sender-${client}`}
          end={`topic-${topic}`}
          onClick={() => setOpen(true)}
        />
      ) : (
        <FlowArrow
          start={`topic-${topic}`}
          end={`receiver-${client.client}`}
          onClick={() => setOpen(true)}
          anim={client.subscribed}
        />
      )}
      <Popover
        open={open}
        onClose={() => setOpen(false)}
        anchorEl={document.getElementById("root")}
        anchorOrigin={{ horizontal: "center", vertical: "center" }}
        transformOrigin={{ horizontal: "center", vertical: "center" }}
      >
        <div className={classes.message_list_popup}>
          <MessageList
            broker={broker}
            filter={
              new AndFilter(
                publication
                  ? new SenderFilter(client)
                  : new ReceiverFilter(client.client),
                new TopicFilter(topic)
              )
            }
          />
        </div>
      </Popover>
    </div>
  );
};

/**
 * Display flows of messages
 *
 * @param {{flow: Map<string, Set<string | {client: string, subscribed: boolean}>>, publication: boolean, broker: BrokerState}} props the flows of sent messages to topic (indicate if subscribed if not publication) and if this is a publication or a forward and the broker state
 */
const Flows = ({ flow, publication, broker }) => {
  return (
    <div>
      {Array.from(flow.entries()).map(([topic, clients_]) => (
        <div key={topic}>
          {[...clients_.values()].map((client) => (
            <Flow
              key={publication ? client : client.client}
              client={client}
              topic={topic}
              publication={publication}
              broker={broker}
            />
          ))}
        </div>
      ))}
    </div>
  );
};

/**
 * Display the flow of message on the broker
 *
 * @param {{broker: BrokerState}} props the state of the broker
 */
const PacketFlow = ({ broker }) => {
  // The flows of messages sent to a topic
  let sentTo = new Map(
    broker.getTopics().map((topic) => [topic.topic, new Set()])
  );
  // The flow of messages fetch from a topic
  let fetchFrom = new Map(
    broker.getTopics().map((topic) => [topic.topic, new Set()])
  );
  broker.getMessages().forEach((message) => {
    sentTo.set(message.topic, sentTo.get(message.topic).add(message.sender));
    broker
      .getTopics()
      .filter((topic) => new TopicFilter(topic.topic).check(message))
      .forEach((topic) =>
        fetchFrom.set(
          topic.topic,
          new Set([...fetchFrom.get(topic.topic), ...message.receivers])
        )
      );
  });
  // Add if a receiver is subscribe to a topic
  broker.getTopics().forEach((topic) => {
    const receivers = new Set([
      ...fetchFrom.get(topic.topic),
      ...topic.subscribedClients,
    ]);
    const newReceivers = new Set();
    receivers.forEach((receiver) => {
      if (topic.subscribedClients.includes(receiver)) {
        newReceivers.add({ client: receiver, subscribed: true });
      } else {
        newReceivers.add({ client: receiver, subscribed: false });
      }
    });
    fetchFrom.set(topic.topic, newReceivers);
  });

  return (
    <div>
      <Flows flow={sentTo} publication={true} broker={broker} />
      <Flows flow={fetchFrom} publication={false} broker={broker} />
    </div>
  );
};

PacketFlow.propTypes = {
  /**
   * The state of the broker
   */
  broker: PropTypes.instanceOf(BrokerState).isRequired,
};

export default PacketFlow;
