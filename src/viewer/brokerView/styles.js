import { createStyles } from "@mui/styles";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles(() =>
  createStyles({
    card_view: {
      margin: "20px",
      height: "100px",
      "&:hover": {
        boxShadow: "0px 0px 50px -1px rgb(0 0 0 / 20%)",
      },
    },

    message_list_popup: {
      backgroundColor: "#DBDBDB",
    },
  })
);

export { useStyles };
