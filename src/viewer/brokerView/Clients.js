import React, { useState } from "react";
import { Card } from "@mui/material";
import { useStyles } from "./styles";
import BrokerState from "../../model/BrokerState";
import PropTypes from "prop-types";
import { Typography } from "@mui/material";
import { Popover } from "@mui/material";
import MessageList from "../packets/MessageList";
import { ReceiverFilter, SenderFilter } from "../../model/filter/BasicFilters";
import { CardContent } from "@mui/material";
import { CompareArrows } from "@mui/icons-material";
import { IconButton } from "@mui/material";
import { QueryStats } from "@mui/icons-material";
import { Grid, Tooltip } from "@mui/material";
import { Visibility } from "@mui/icons-material";
import Telemetry from "../packets/Telemetry";

/**
 * Display the boxs for each MQTT client with the ids
 *
 * @param {{broker: BrokerState, sender: boolean}} props the state of the broker and if the clients are senders
 */
const Clients = ({ broker, sender = true }) => {
  const classes = useStyles();

  // The client selected by the client
  const [open, setOpen] = useState(undefined);

  // The type of popup selected but the user
  const [type, setType] = useState("messages");

  return broker.getClients().map((client) => (
    <div key={client.id}>
      <Card
        variant="elevation"
        id={`${sender ? "sender" : "receiver"}-${client.id}`}
        className={classes.card_view}
      >
        <CardContent>
          <Grid container>
            <Grid item md={10}>
              <Typography variant="h5" color="secondary" align="center" noWrap>
                <CompareArrows
                  color={client.isConnected() ? "success" : "error"}
                />{" "}
                {client.id}
              </Typography>
              <Typography variant="body2" color="primary" align="center">
                ({client.ip})
              </Typography>
            </Grid>
            <Grid
              item
              md={2}
              container
              direction="column"
              justifyContent="space-around"
              style={{ height: "100%" }}
            >
              <Tooltip title="See packets" disableInteractive arrow>
                <IconButton
                  size="small"
                  color="primary"
                  onClick={() => {
                    setOpen(client.id);
                    setType("messages");
                  }}
                >
                  <Visibility />
                </IconButton>
              </Tooltip>
              <Tooltip title="Open telemetry" disableInteractive arrow>
                <IconButton
                  size="small"
                  color="primary"
                  onClick={() => {
                    setOpen(client.id);
                    setType("telemetry");
                  }}
                >
                  <QueryStats />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <Popover
        open={open === client.id}
        onClose={() => setOpen(undefined)}
        anchorEl={document.getElementById("root")}
        anchorOrigin={{ horizontal: "center", vertical: "center" }}
        transformOrigin={{ horizontal: "center", vertical: "center" }}
      >
        {type === "messages" ? (
          <div className={classes.message_list_popup}>
            <MessageList
              broker={broker}
              filter={
                sender
                  ? new SenderFilter(client.id)
                  : new ReceiverFilter(client.id)
              }
            />
          </div>
        ) : (
          <Telemetry
            broker={broker}
            filter={
              sender
                ? new SenderFilter(client.id)
                : new ReceiverFilter(client.id)
            }
          />
        )}
      </Popover>
    </div>
  ));
};

Clients.propTypes = {
  /**
   * The state of the broker
   */
  broker: PropTypes.instanceOf(BrokerState).isRequired,

  /**
   * If the clients to represent are senders
   */
  sender: PropTypes.bool,
};

export default Clients;
