import { Card } from "@mui/material";
import React, { useState } from "react";
import { useStyles } from "./styles";
import PropTypes from "prop-types";
import { Typography } from "@mui/material";
import BrokerState from "../../model/BrokerState";
import { Popover } from "@mui/material";
import MessageList from "../packets/MessageList";
import { TopicFilter } from "../../model/filter/BasicFilters";
import { CardContent, Grid, IconButton, Tooltip } from "@mui/material";
import { QueryStats } from "@mui/icons-material";
import { Visibility } from "@mui/icons-material";
import Telemetry from "../packets/Telemetry";

/**
 * Display the list of topics
 *
 * @param {{broker: BrokerState}} props the state of the broker
 */
const Topics = ({ broker }) => {
  const classes = useStyles();

  // The topic selected by the user
  const [open, setOpen] = useState(undefined);

  // The type of popup selected but the user
  const [type, setType] = useState("messages");

  return broker.getTopics().map((topic) => (
    <div key={topic.topic}>
      <Card
        variant="elevation"
        id={`topic-${topic.topic}`}
        className={classes.card_view}
      >
        <CardContent>
          <Grid container>
            <Grid item md={10}>
              <Typography variant="h5" color="secondary" align="center" noWrap>
                {topic.topic}
              </Typography>
            </Grid>
            <Grid
              item
              md={2}
              container
              direction="column"
              justifyContent="space-around"
              style={{ height: "100%" }}
            >
              <Tooltip title="See packets" disableInteractive arrow>
                <IconButton
                  size="small"
                  color="primary"
                  onClick={() => {
                    setOpen(topic.topic);
                    setType("messages");
                  }}
                >
                  <Visibility />
                </IconButton>
              </Tooltip>
              <Tooltip title="Open telemetry" disableInteractive arrow>
                <IconButton
                  size="small"
                  color="primary"
                  onClick={() => {
                    setOpen(topic.topic);
                    setType("telemetry");
                  }}
                >
                  <QueryStats />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <Popover
        open={open === topic.topic}
        onClose={() => setOpen(undefined)}
        anchorEl={document.getElementById("root")}
        anchorOrigin={{ horizontal: "center", vertical: "center" }}
        transformOrigin={{ horizontal: "center", vertical: "center" }}
      >
        {type === "messages" ? (
          <div className={classes.message_list_popup}>
            <MessageList
              broker={broker}
              filter={new TopicFilter(topic.topic)}
            />
          </div>
        ) : (
          <Telemetry broker={broker} filter={new TopicFilter(topic.topic)} />
        )}
      </Popover>
    </div>
  ));
};

Topics.propTypes = {
  /**
   * The broker state
   */
  broker: PropTypes.instanceOf(BrokerState).isRequired,
};

export default Topics;
