import { ContentCopy } from "@mui/icons-material";
import { TabPanel } from "@mui/lab";
import { TabContext } from "@mui/lab";
import { Alert } from "@mui/lab";
import { TabList } from "@mui/lab";
import { IconButton } from "@mui/material";
import { Snackbar } from "@mui/material";
import { Tab } from "@mui/material";
import { Divider } from "@mui/material";
import { Button, Typography, Tooltip } from "@mui/material";
import { Box } from "@mui/system";
import React, { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";
import BrokerView from "./brokerView/BrokerView";
import MessageList from "./packets/MessageList";
import Telemetry from "./packets/Telemetry";
import BrokerState from "../model/BrokerState";
import fetchPlugins from "../plugins/loader";

/**
 * Display the list of published messages
 */
const Published = () => {
  /**
   * The ID of the broker (the username)
   *
   * @type {string}
   */
  const brokerID = useParams().brokerID;

  /**
   * The previous brokerID used
   */
  const prevBrokerID = useRef(brokerID);

  /**
   * The state of the broker
   *
   * @type {[BrokerState, React.Dispatch<React.SetStateAction<BrokerState>>]}
   */
  const [brokerState, setBrokerState] = useState(new BrokerState(brokerID));

  /**
   * Which tab is selected
   */
  const [tabValue, setTabValue] = useState("0");

  /**
   * The list of additional features
   *
   * @type {[[{name: string, import: JSX.Element}], React.Dispatch<React.SetStateAction<[{name: string, import: JSX.Element}]>>]}
   */
  const [additionalFeatures, setAdditionalFeatures] = useState([]);

  /**
   * Which error occured
   */
  const [error, setError] = useState(0);

  /**
   * The feedback for the clipboard
   */
  const [feedback, setFeedback] = useState(0);

  useEffect(() => {
    // Check if we change the brokerID. If yes, reinitialize everything
    if (brokerID !== prevBrokerID.current) {
      setBrokerState(new BrokerState(brokerID));
      prevBrokerID.current = brokerID;
    }

    // Update the broker state avery seconds
    const clockId = setInterval(async () => {
      const newState = await brokerState.update();
      if (typeof newState === "number") {
        setError(newState);
      } else {
        setBrokerState(newState);
        setError(0);
      }
    }, 1000);

    return () => clearInterval(clockId);
  });

  useEffect(async () => {
    // Add the new features provided by plugins to the panels
    const additionalFeaturePlugins = await Promise.all(
      await fetchPlugins("additionalFeature")
    );

    setAdditionalFeatures(additionalFeaturePlugins);
  }, []);

  // Error if the broker does not exist
  if (error === 1) {
    return (
      <Typography variant="h4" color="error">
        The broker {brokerID} does not exist
      </Typography>
    );
  }

  // Error if network or server error
  if (error === 2) {
    return (
      <Typography variant="h4" color="error">
        Network error
      </Typography>
    );
  }

  return (
    <div>
      <Link to="/">
        <Button variant="outlined" color="primary">
          Home
        </Button>
      </Link>
      <Typography variant="h4" color="primary">
        Username to use: {brokerID}
        <Tooltip title="Copy" disableInteractive arrow>
          <IconButton
            size="large"
            color="secondary"
            onClick={() => {
              if (window.isSecureContext) {
                // Copy to clipboard the broker ID
                navigator.clipboard
                  .writeText(brokerID)
                  .then(() => setFeedback(1));
              } else {
                setFeedback(2);
              }
            }}
          >
            <ContentCopy />
          </IconButton>
        </Tooltip>
      </Typography>

      <Divider variant="middle" />
      <TabContext value={tabValue}>
        <Box sx={{ borderBottom: 1, borderColor: "GrayText" }}>
          <TabList onChange={(e, v) => setTabValue(v)}>
            <Tab label="All" value="0" />
            <Tab label="Broker View" value="1" />
            <Tab label="Telemetry" value="2" />
            {additionalFeatures.map(({ name }) => (
              <Tab key={name} label={name} value={name} />
            ))}
          </TabList>
        </Box>
        <TabPanel value="0">
          <MessageList broker={brokerState} />
        </TabPanel>
        <TabPanel value="1">
          <BrokerView broker={brokerState} />
        </TabPanel>
        <TabPanel value="2">
          <Telemetry broker={brokerState} />
        </TabPanel>
        {/* The additional features panels */}
        {additionalFeatures.map((feature) => (
          <TabPanel key={feature.name} value={feature.name}>
            <feature.import broker={brokerState} />
          </TabPanel>
        ))}
      </TabContext>

      <Snackbar
        onClose={() => setFeedback(0)}
        open={feedback !== 0}
        autoHideDuration={3000}
      >
        <Alert severity={feedback === 1 ? "success" : "error"}>
          {feedback === 1 ? "Copied!" : "Not secure session!"}
        </Alert>
      </Snackbar>
    </div>
  );
};

export default Published;
