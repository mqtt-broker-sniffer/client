import { List } from "@mui/material";
import { ListItem } from "@mui/material";
import { Typography } from "@mui/material";
import { Checkbox } from "@mui/material";
import { ListItemText } from "@mui/material";
import { Grid } from "@mui/material";
import React, { useEffect, useState } from "react";
import BrokerState from "../../../model/BrokerState";
import { useStyles } from "./styles";
import PropTypes from "prop-types";
import { FormControlLabel } from "@mui/material";
import {
  QosFilter,
  ReceiverFilter,
  SenderFilter,
  TopicFilter,
} from "../../../model/filter/BasicFilters";
import { AndFilter, OrFilter } from "../../../model/filter/OperatorFilters";
import Filter from "../../../model/filter/Filter";

/**
 * The selector for the client filters
 *
 * @param {{onChange: (newFilter: Filter) => void, clients: [Client], senders: boolean}} props the callback when a new filter is created and the list of clients and if the clients are senders
 */
const ClientSelector = ({ onChange, clients, senders }) => {
  const classes = useStyles();

  // The selected clients
  const [selected, setSelected] = useState(
    senders ? clients.map((client) => client.id) : []
  );

  /**
   * Check a checkbox change
   *
   * @param {Client} client the client with this checkbox
   * @param {boolean} checked if the checkbox is checked or unchecked
   */
  const changeCheckbox = (client, checked) => {
    setSelected(
      checked
        ? [...selected, client.id]
        : [
            ...selected.slice(0, selected.indexOf(client.id)),
            ...selected.slice(selected.indexOf(client.id) + 1),
          ]
    );
  };

  // Call onChange when the state is changed
  useEffect(() => {
    onChange(
      selected
        .map((clientId) =>
          senders ? new SenderFilter(clientId) : new ReceiverFilter(clientId)
        )
        .reduce(
          (prevFilter, curFilter) =>
            senders
              ? new OrFilter(prevFilter, curFilter)
              : new AndFilter(prevFilter, curFilter),
          new Filter(!senders)
        )
    );
  }, [selected]);

  return (
    <List className={classes.client_selector} dense disablePadding>
      {clients.map((client) => (
        <ListItem
          key={client.id}
          secondaryAction={
            <Checkbox
              edge="end"
              color="secondary"
              checked={selected.includes(client.id)}
              onChange={(e) => changeCheckbox(client, e.target.checked)}
            />
          }
        >
          <ListItemText
            primary={
              <Typography variant="h6" color="primary" noWrap>
                {client.id}
              </Typography>
            }
            secondary={
              <Typography variant="body2" color="secondary">
                {client.ip}
              </Typography>
            }
          />
        </ListItem>
      ))}
    </List>
  );
};

/**
 * The selector for the topic filters
 *
 * @param {{onChange: (newFilter: Filter) => void, topics: [Topic]}} props the callback when a new filter is created and the list of topics
 */
const TopicSelector = ({ onChange, topics }) => {
  const classes = useStyles();

  // The selected topics
  const [selected, setSelected] = useState(topics.map((topic) => topic.topic));

  /**
   * Check a checkbox change
   *
   * @param {Topic} topic the topic with this checkbox
   * @param {boolean} checked if the checkbox is checked or unchecked
   */
  const changeCheckbox = (topic, checked) => {
    setSelected(
      checked
        ? [...selected, topic.topic]
        : [
            ...selected.slice(0, selected.indexOf(topic.topic)),
            ...selected.slice(selected.indexOf(topic.topic) + 1),
          ]
    );
  };

  // Call onChange when the state is changed
  useEffect(() => {
    onChange(
      selected
        .map((topicName) => new TopicFilter(topicName))
        .reduce(
          (prevFilter, curFilter) => new OrFilter(prevFilter, curFilter),
          new Filter(false)
        )
    );
  }, [selected]);

  return (
    <List className={classes.client_selector} dense disablePadding>
      {topics.map((topic) => (
        <ListItem
          key={topic.topic}
          secondaryAction={
            <Checkbox
              edge="end"
              color="secondary"
              checked={selected.includes(topic.topic)}
              onChange={(e) => changeCheckbox(topic, e.target.checked)}
            />
          }
        >
          <ListItemText
            primary={
              <Typography variant="h6" color="primary" noWrap>
                {topic.topic}
              </Typography>
            }
          />
        </ListItem>
      ))}
    </List>
  );
};

/**
 * The selector for the QOS filters
 *
 * @param {{onChange: (newFilter: Filter) => void}} props the callback when a new filter is created
 */
const QosSelector = ({ onChange }) => {
  const QOS = [0, 1, 2];

  // The selected clients
  const [selected, setSelected] = useState(QOS);

  /**
   * Check a checkbox change
   *
   * @param {number} qos the QOS with this checkbox
   * @param {boolean} checked if the checkbox is checked or unchecked
   */
  const changeCheckbox = (qos, checked) => {
    setSelected(
      checked
        ? [...selected, qos]
        : [
            ...selected.slice(0, selected.indexOf(qos)),
            ...selected.slice(selected.indexOf(qos) + 1),
          ]
    );
  };

  // Call onChange when the state is changed
  useEffect(() => {
    onChange(
      selected
        .map((qos_) => new QosFilter(qos_))
        .reduce(
          (prevFilter, curFilter) => new OrFilter(prevFilter, curFilter),
          new Filter(false)
        )
    );
  }, [selected]);

  return (
    <Grid container justifyContent="space-around">
      {QOS.map((qos) => (
        <FormControlLabel
          key={qos}
          label={qos}
          labelPlacement="end"
          control={
            <Checkbox
              color="secondary"
              checked={selected.includes(qos)}
              onChange={(e) => changeCheckbox(qos, e.target.checked)}
            />
          }
        />
      ))}
    </Grid>
  );
};

/**
 * The default filter creator
 *
 * @param {{onChange: (newFilter: Filter) => void, broker: BrokerState}} props the callback when a new filter is created and the broker state
 */
const DefaultFilter = ({ onChange = (_) => {}, broker }) => {
  // The sender filter
  const [senderFilter, setSenderFilter] = useState(new Filter());
  // The receiver filter
  const [receiverFilter, setReceiverFilter] = useState(new Filter());
  // The QOS filter
  const [qosFilter, setQosFilter] = useState(new Filter());
  // The topic filter
  const [topicFilter, setTopicFilter] = useState(new Filter());

  /**
   * Create a new filter for the on change
   */
  const updateOnChange = () => {
    onChange(
      new AndFilter(
        new AndFilter(senderFilter, receiverFilter),
        new AndFilter(qosFilter, topicFilter)
      )
    );
  };

  useEffect(updateOnChange, [
    senderFilter,
    receiverFilter,
    qosFilter,
    topicFilter,
  ]);

  return (
    <Grid container spacing={3}>
      <Grid item>
        <Typography variant="h6" color="primary">
          Select senders:
        </Typography>
        <ClientSelector
          clients={broker.getClients()}
          senders={true}
          onChange={(filter) => {
            setSenderFilter(filter);
          }}
        />
      </Grid>
      <Grid item>
        <Typography variant="h6" color="primary">
          Select receivers:
        </Typography>
        <ClientSelector
          clients={broker.getClients()}
          senders={false}
          onChange={(filter) => {
            setReceiverFilter(filter);
          }}
        />
      </Grid>
      <Grid item>
        <Typography variant="h6" color="primary">
          Select topics:
        </Typography>
        <TopicSelector
          topics={broker.getTopics()}
          onChange={(filter) => {
            setTopicFilter(filter);
          }}
        />
      </Grid>
      <Grid item>
        <Typography variant="h6" color="primary">
          Select QOS:
        </Typography>
        <QosSelector
          onChange={(filter) => {
            setQosFilter(filter);
          }}
        />
      </Grid>
    </Grid>
  );
};

DefaultFilter.propTypes = {
  /**
   * The callback when a new filter is created
   */
  onChange: PropTypes.func,

  /**
   * The list of clients
   */
  broker: PropTypes.instanceOf(BrokerState).isRequired,
};

export default DefaultFilter;
