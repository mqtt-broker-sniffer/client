import { createStyles } from "@mui/styles";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles(() =>
  createStyles({
    panel_filter: {
      padding: "5px",
      width: "300px",
      height: "100%",
      backgroundColor: "#C3C9C9",
      overflow: "auto",
    },

    client_selector: {
      width: "280px",
      height: "250px",
      borderStyle: "solid",
      borderRadius: 5,
      borderWidth: 1,
      overflow: "auto",
    },
  })
);

export { useStyles };
