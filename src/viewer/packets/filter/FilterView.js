import React, { useEffect, useState } from "react";
import Filter from "../../../model/filter/Filter";
import { Drawer } from "@mui/material";
import PropTypes from "prop-types";
import { Box } from "@mui/material";
import { useStyles } from "./styles";
import BrokerState from "../../../model/BrokerState";
import DefaultFilter from "./DefaultFilter";
import fetchPlugins from "../../../plugins/loader";
import ContentFilterViewer from "./ContentFilterViewer";
import { AndFilter, OrFilter } from "../../../model/filter/OperatorFilters";
import ContentFilter from "../../../model/filter/ContentFilter";
import { Accordion } from "@mui/material";
import { AccordionSummary } from "@mui/material";
import { Checkbox } from "@mui/material";
import { Typography } from "@mui/material";

/**
 * A filter window to create filters for the messages
 *
 * @param {{onChange: (newFilter: Filter) => void, onClose: () => void, open: boolean, broker: BrokerState}} props the callback when a new filter is created and a callback when the window is closed and of the filter is open and the broker state
 */
const FilterView = ({
  onChange = (_) => {},
  onClose = () => {},
  open,
  broker,
}) => {
  const classes = useStyles();

  // If the panel has already been opened
  const [alreadyOpened, setAlreaderOpened] = useState(false);
  // The filter plugins
  const [plugins, setPlugins] = useState(undefined);
  // The content filters
  const [contentFilters, setContentFilters] = useState([]);
  // The default filter
  const [defaultFilter, setDefaultFilter] = useState(new Filter());
  // If the filter to accect the remaining types of packets is set
  const [acceptOthers, setAcceptOthers] = useState(true);

  useEffect(() => {
    // Update the fact that the panel has already been opened
    if (open && !alreadyOpened) {
      setAlreaderOpened(true);
    }
  });

  useEffect(() => {
    // Call the onChange when a filter is changed
    onChange(
      [
        ...contentFilters,
        ...(acceptOthers || plugins === undefined
          ? []
          : [
              plugins
                .map((plugin) => new ContentFilter(plugin.verifier))
                .reduce(
                  (filter1, filter2) => new OrFilter(filter1, filter2),
                  new Filter(false)
                ),
            ]),
      ].reduce(
        (filter1, filter2) => new AndFilter(filter1, filter2),
        defaultFilter
      )
    );
  }, [contentFilters, defaultFilter, acceptOthers]);

  useEffect(async () => {
    // Import the plugins
    const messageFiltersPlugins = await Promise.all(
      await fetchPlugins("messageFilters")
    );
    const contentVerifierPlugins = await Promise.all(
      await fetchPlugins("messageContentVerifier")
    );

    const loadedPlugins = messageFiltersPlugins
      .map((filterPlugin) => {
        const verifier = contentVerifierPlugins.find(
          (verifierPlugin) => filterPlugin.name === verifierPlugin.name
        );
        return {
          filter: filterPlugin.import,
          verifier: verifier === undefined ? undefined : verifier.import,
          name: filterPlugin.name,
        };
      })
      .filter((plugin) => plugin.verifier !== undefined);

    setPlugins(loadedPlugins);
    setContentFilters(loadedPlugins.map(() => new Filter()));
  }, []);

  // Do not launch the state of the DOM when the panel has never been opened
  if (!alreadyOpened) {
    return <div />;
  }

  return (
    <Drawer
      open={open}
      onClose={onClose}
      anchor="right"
      ModalProps={{
        keepMounted: true,
        container: document.getElementById("message-list"),
      }}
    >
      <Box role="presentation" className={classes.panel_filter}>
        <DefaultFilter
          broker={broker}
          onChange={(filter) => setDefaultFilter(filter)}
        />
      </Box>
      {plugins.map((plugin, index) => (
        <ContentFilterViewer
          key={plugin.name}
          plugin={plugin}
          onChange={(filter) =>
            setContentFilters([
              ...contentFilters.slice(0, index),
              filter,
              ...contentFilters.slice(index + 1),
            ])
          }
        />
      ))}
      <Accordion disableGutters>
        <AccordionSummary>
          <Typography variant="h6" color="primary">
            <Checkbox
              checked={acceptOthers}
              onChange={(e) => setAcceptOthers(e.target.checked)}
              color="secondary"
            />
            Other types of content
          </Typography>
        </AccordionSummary>
      </Accordion>
    </Drawer>
  );
};

FilterView.propTypes = {
  /**
   * The callback when a new filter is created
   */
  onChange: PropTypes.func,

  /**
   * The callback when the panel is closed
   */
  onClose: PropTypes.func,

  /**
   * If the panel is closed
   */
  open: PropTypes.bool.isRequired,

  /**
   * The broker state
   */
  broker: PropTypes.instanceOf(BrokerState).isRequired,
};

export default FilterView;
