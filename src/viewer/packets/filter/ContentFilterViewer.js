import React, { useEffect, useState } from "react";
import Filter from "../../../model/filter/Filter";
import { Accordion, AccordionSummary, AccordionDetails } from "@mui/material";
import { ExpandMore } from "@mui/icons-material";
import PropTypes from "prop-types";
import { Typography } from "@mui/material";
import { Checkbox } from "@mui/material";
import { NotFilter, OrFilter } from "../../../model/filter/OperatorFilters";
import ContentFilter from "../../../model/filter/ContentFilter";

/**
 * The viewer if the filter of the content for a plugin filter
 *
 * @param {{onChange: (newFilter: Filter) => void, plugin: {verifier: (content: Buffer) => boolean, filter: JSX.Element, name: string}}} props the callback when the filter is changed and the plugin
 */
const ContentFilterViewer = ({ onChange = (_) => {}, plugin }) => {
  // If the type of content is accepted
  const [acceptType, setAcceptType] = useState(true);
  // The filter of the content
  const [filter, setFilter] = useState(new Filter());

  // Update the onChange when the filter change
  useEffect(() => {
    onChange(
      acceptType
        ? new OrFilter(
            new NotFilter(new ContentFilter(plugin.verifier)),
            filter
          )
        : new NotFilter(new ContentFilter(plugin.verifier))
    );
  }, [acceptType, filter]);

  return (
    <Accordion disableGutters>
      <AccordionSummary expandIcon={<ExpandMore />}>
        <Typography variant="h6" color="primary">
          <Checkbox
            checked={acceptType}
            onChange={(e) => setAcceptType(e.target.checked)}
            color="secondary"
          />
          {plugin.name}
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <plugin.filter onChange={(newFilter) => setFilter(newFilter)} />
      </AccordionDetails>
    </Accordion>
  );
};

ContentFilterViewer.propTypes = {
  /**
   * The callback when the filter is changed
   */
  onChange: PropTypes.func,

  /**
   * The plugin
   */
  plugin: PropTypes.object.isRequired,
};

export default ContentFilterViewer;
