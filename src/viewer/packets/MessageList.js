import React, { useEffect, useRef, useState } from "react";
import MQTTMessage from "./message/MQTTMessage";
import PropTypes from "prop-types";
import Filter from "../../model/filter/Filter";
import { CellMeasurerCache } from "react-virtualized";
import { List } from "react-virtualized";
import { CellMeasurer } from "react-virtualized";
import { Grid, IconButton } from "@mui/material";
import { ArrowDownward } from "@mui/icons-material";
import { QueryStats } from "@mui/icons-material";
import { Popover, Tooltip } from "@mui/material";
import Telemetry from "./Telemetry";
import { FilterAlt } from "@mui/icons-material";
import FilterView from "./filter/FilterView";
import BrokerState from "../../model/BrokerState";
import { AndFilter } from "../../model/filter/OperatorFilters";

const cache = new CellMeasurerCache({
  defaultHeight: 50,
  fixedWidth: true,
});

/**
 * Display a list of MQTT messages
 *
 * @param {{broker: BrokerState, filter: Filter}} props broker: the broker state, filter: the filter to use on the messages
 */
const MessageList = ({ broker, filter = new Filter() }) => {
  const ref = useRef();
  // The current top coordinate of the current element
  const [currentTop, setCurrentTop] = useState(0);
  // If we should follow the last MQTT messages
  const [followLast, setFollowLast] = useState(true);
  // If we allow to reset the cache to have a good render of the message
  const [allowReset, setAllowReset] = useState(true);
  // If the popup for the telemetry is open
  const [open, setOpen] = useState(false);
  // If the option panel is open
  const [optionOpen, setOption] = useState(false);
  // The filter of the filter panel
  const [panelFilter, setPanelFilter] = useState(new Filter());

  // The filtered list of messages
  const filtered = new AndFilter(filter, panelFilter).filter(
    broker.getMessages()
  );

  //The view of the message
  const Message = ({ index, key, style, parent }) => (
    <CellMeasurer
      cache={cache}
      columnIndex={0}
      key={key}
      parent={parent}
      rowIndex={index}
    >
      <div style={style}>
        <MQTTMessage message={filtered[index]} />
      </div>
    </CellMeasurer>
  );

  useEffect(() => {
    setCurrentTop(ref.current.getBoundingClientRect().top);

    const clockId = setInterval(() => {
      // Allow every seconds to reset the cache to have a good render
      setAllowReset(true);
    }, 100);

    return () => clearInterval(clockId);
  }, []);

  return (
    <Grid container id="message-list">
      <Grid item md={11}>
        <div
          ref={ref}
          style={{
            width: 1000,
            height: window.innerHeight - currentTop - 25,
            overflow: "auto",
            alignContent: "center",
          }}
        >
          {filtered.length > 10 ? (
            <List
              width={1000}
              height={window.innerHeight - currentTop - 25}
              rowCount={filtered.length}
              deferredMeasurementCache={cache}
              rowHeight={cache.rowHeight}
              rowRenderer={Message}
              overscanRowCount={0}
              onRowsRendered={(rows) => {
                // Made to disable multiple reset of the cache because this event is called a lot
                if (allowReset) {
                  for (let i = rows.startIndex; i <= rows.stopIndex; i++) {
                    cache.clear(i, 0);
                  }
                  setAllowReset(false);
                }
              }}
              scrollToIndex={followLast ? filtered.length - 1 : undefined}
              scrollToAlignment="end"
            />
          ) : (
            filtered.map((message) => (
              <div key={message.serialNumber}>
                <MQTTMessage message={message} />
              </div>
            ))
          )}
        </div>
      </Grid>
      <Grid
        item
        md={1}
        container
        direction="column"
        justifyContent="flex-end"
        alignItems="flex-end"
      >
        <Tooltip title="Open filters" disableInteractive arrow>
          <IconButton onClick={() => setOption(true)} color="secondary">
            <FilterAlt />
          </IconButton>
        </Tooltip>
        <Tooltip title="Open telemetry" disableInteractive arrow>
          <IconButton onClick={() => setOpen(true)} color="secondary">
            <QueryStats />
          </IconButton>
        </Tooltip>
        <Tooltip
          title={followLast ? "Stop following" : "Go down"}
          disableInteractive
          arrow
        >
          <IconButton
            onClick={() => setFollowLast(!followLast)}
            color={followLast ? "secondary" : "default"}
          >
            <ArrowDownward />
          </IconButton>
        </Tooltip>
      </Grid>

      {/* The popup of the telemetry */}
      <Popover
        open={open}
        onClose={() => setOpen(false)}
        anchorEl={document.getElementById("root")}
        anchorOrigin={{ horizontal: "center", vertical: "center" }}
        transformOrigin={{ horizontal: "center", vertical: "center" }}
      >
        <Telemetry
          broker={broker}
          filter={new AndFilter(filter, panelFilter)}
        />
      </Popover>

      {/* The panel for the filters */}
      <FilterView
        onClose={() => setOption(false)}
        open={optionOpen}
        broker={broker}
        onChange={(newFilter) => setPanelFilter(newFilter)}
      />
    </Grid>
  );
};

MessageList.propTypes = {
  /**
   * The state of the broker
   */
  broker: PropTypes.instanceOf(BrokerState).isRequired,

  /**
   * The filter to apply on the messages
   */
  filter: PropTypes.instanceOf(Filter),
};

export default MessageList;
