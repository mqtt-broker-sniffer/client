import { Grid, MenuItem, Paper, Select, Typography } from "@mui/material";
import React, { useState } from "react";
import PropTypes from "prop-types";

const encodingMethods = [
  "ascii",
  "utf8",
  "utf16le",
  "ucs2",
  "base64",
  "latin1",
  "binary",
  "hex",
];

/**
 * The default displayer of the content of a MQTT message
 *
 * @param {{content: Buffer}} props the content of the MQTT message
 */
const DefaultContentView = ({ content }) => {
  // The encoder selected by the user
  const [selectedMethod, setMethod] = useState(1);

  return (
    <Grid container>
      <Grid item md={11}>
        <Paper variant="outlined">
          <Typography variant="body2">
            {content.toString(encodingMethods[selectedMethod])}
          </Typography>
        </Paper>
      </Grid>
      <Grid item md={1}>
        <Select
          variant="outlined"
          label="Encoding"
          onChange={(e) => setMethod(e.target.value)}
          value={selectedMethod}
        >
          {encodingMethods.map((method, index) => (
            <MenuItem key={index} value={index}>
              {method}
            </MenuItem>
          ))}
        </Select>
      </Grid>
    </Grid>
  );
};

DefaultContentView.propTypes = {
  /**
   * The content of the message
   */
  content: PropTypes.instanceOf(Buffer).isRequired,
};

export default DefaultContentView;
