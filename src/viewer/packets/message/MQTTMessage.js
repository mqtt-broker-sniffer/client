import { Card, CardContent, Grid, Typography } from "@mui/material";
import React from "react";
import { Message } from "../../../model/Message";
import PropTypes from "prop-types";
import { createStyles } from "@mui/styles";
import { makeStyles } from "@mui/styles";
import MessageContent from "./MessageContent";

const useStyles = makeStyles(() =>
  createStyles({
    message: {
      margin: "5px",
    },
  })
);

/**
 * Display the content of a MQTT message
 *
 * @param {{message: Message}} props the message to display
 */
const MQTTMessage = ({ message }) => {
  const classes = useStyles();

  return (
    <Card variant="elevation" className={classes.message}>
      <CardContent>
        <Grid container spacing={3} alignContent="start">
          <Grid item md={6} justifyContent="space-between">
            <Typography variant="body1">Topic: {message.topic}</Typography>
          </Grid>
          <Grid item md={6} justifyContent="space-between">
            <Typography variant="body1">Sender: {message.sender}</Typography>
          </Grid>
          <Grid item md={6}>
            <Typography variant="body1">
              Receivers: {message.receivers.join(", ")}
            </Typography>
          </Grid>
          <Grid item md={5}>
            <Typography variant="body1">
              Time: {message.time.toFormat("dd LLL yyyy',' HH':'mm':'ss':'SSS")}
            </Typography>
          </Grid>
          <Grid item md={1}>
            <Typography variant="body1">QOS: {message.qos}</Typography>
          </Grid>
          <Grid item md={12}>
            <MessageContent content={message.content} />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

MQTTMessage.propTypes = {
  /**
   * The message to display
   */
  message: PropTypes.instanceOf(Message).isRequired,
};

export default MQTTMessage;
