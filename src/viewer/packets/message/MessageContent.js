import React, { useEffect, useState } from "react";
import fetchPlugins from "../../../plugins/loader";
import DefaultContentView from "./DefaultContentView";
import PropTypes from "prop-types";

/**
 * Display the content of the message with the appropriate display
 *
 * @param {{content: Buffer}} props the content of the message
 */
const MessageContent = ({ content }) => {
  // The render of the content of the message
  const [render, rerender] = useState(<div />);

  useEffect(async () => {
    const contentViewersPlugins = await fetchPlugins("messageContentViewer");
    const contentVerifierPlugins = await fetchPlugins("messageContentVerifier");

    for (const promisedPlugin of contentViewersPlugins) {
      const plugin = await promisedPlugin;
      // Verify if there is a verifier from the same plugin and test the content
      let verifierPlugin;
      for (const verifPlugin of contentVerifierPlugins) {
        const { name } = await verifPlugin;
        if (name === plugin.name) {
          verifierPlugin = await verifPlugin;
        }
      }
      if (verifierPlugin !== undefined && verifierPlugin.import(content)) {
        const Viewer = plugin.import;
        rerender(<Viewer content={content} />);
        return;
      }
    }
    // If not plugin work, consider as text
    rerender(<DefaultContentView content={content} />);
  }, [content]);

  return render;
};

MessageContent.propTypes = {
  /**
   * The content of the message
   */
  content: PropTypes.instanceOf(Buffer).isRequired,
};

export default MessageContent;
