import React, { useEffect, useRef, useState } from "react";
import Filter from "../../model/filter/Filter";
import BrokerState from "../../model/BrokerState";
import PropTypes from "prop-types";
import Chart from "chart.js/auto";
import "chartjs-adapter-luxon";

/**
 * Display the telemetry of the number of messages
 *
 * @param {{broker: BrokerState, filter: Filter}} props the state of the broker and a filter
 */
const Telemetry = ({ broker, filter = new Filter() }) => {
  const chartContainer = useRef(null);
  // The instance of the chart
  const [chartInstance, setChartInstance] = useState(null);
  // The current top coordinate of the current element
  const [currentTop, setCurrentTop] = useState(0);

  // The filtered messages
  const filtered = filter.filter(broker.getMessages());

  // The count of messages per minute
  const minutes = new Map();
  filtered.forEach((message) => {
    const minute = message.time.startOf("minute").toISO();
    if (minutes.has(minute)) {
      minutes.set(minute, minutes.get(minute) + 1);
    } else {
      minutes.set(minute, 1);
    }
  });

  const data = Array.from(minutes.entries()).map(([minute, count]) => ({
    x: minute,
    y: count,
  }));

  const average = `(average: ${
    filtered.length > 0
      ? Math.round(
          filtered.length /
            filtered[filtered.length - 1].time
              .diff(filtered[0].time, "minutes")
              .toObject().minutes
        )
      : "N/A"
  } messages/min)`;

  useEffect(() => {
    setCurrentTop(chartContainer.current.getBoundingClientRect().top);

    // Create the chart
    if (chartContainer && chartContainer.current) {
      const newChartInstance = new Chart(chartContainer.current, {
        type: "line",
        data: {
          datasets: [
            {
              label: `Messages per minute ${average}`,
              fill: false,
              backgroundColor: "rgba(153, 102, 255, 0.5)",
              borderColor: "rgb(153, 102, 255)",
              data,
            },
          ],
        },
        options: {
          scales: {
            x: {
              type: "time",
              display: true,
              time: {
                tooltipFormat: "DD t",
                minUnit: "minute",
              },
              title: {
                display: true,
                text: "Time",
                color: "rgb(54, 162, 235)",
              },
              ticks: {
                autoSkip: false,
                maxRotation: 0,
                major: {
                  enabled: true,
                },
                font: function (context) {
                  if (context.tick && context.tick.major) {
                    return {
                      weight: "bold",
                    };
                  }
                },
              },
            },
            y: {
              title: {
                display: true,
                text: "Messages per minute",
                color: "rgb(54, 162, 235)",
              },
            },
          },
        },
      });
      setChartInstance(newChartInstance);
    }
  }, [chartContainer]);

  useEffect(() => {
    if (chartInstance !== null) {
      // Update the chart
      chartInstance.data.datasets[0].data = data;
      chartInstance.data.datasets[0].label = `Messages per minute ${average}`;
      chartInstance.update();
    }
  }, [broker, filter]);

  return (
    <div>
      <canvas
        style={{ width: 1000, height: window.innerHeight - currentTop - 25 }}
        ref={chartContainer}
      />
    </div>
  );
};

Telemetry.propTypes = {
  /**
   * The state of the broker
   */
  broker: PropTypes.instanceOf(BrokerState).isRequired,

  /**
   * The filter to select the wanted
   */
  filter: PropTypes.instanceOf(Filter),
};

export default Telemetry;
