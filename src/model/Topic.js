/**
 * A MQTT topic
 */
class Topic {
  /**
   * Create a topic
   *
   * @param {string} name the name of the topic
   * @param {[string]} subscribed the list of subscribed clients
   */
  constructor(name, subscribed) {
    this.name_ = name;
    this.subscribed_ = subscribed;
  }

  /**
   * Get the topic name
   *
   * @return {string} the name
   */
  get topic() {
    return this.name_;
  }

  /**
   * Return the list of subsribed clients to the topic
   *
   * @returns {[string]} the ids of subscribed clients
   */
  get subscribedClients() {
    return this.subscribed_;
  }
}

export default Topic;
