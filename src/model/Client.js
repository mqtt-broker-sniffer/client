/**
 * A MQTT client
 */
export default class Client {
  /**
   * Build a MQTT client
   *
   * @param {string} id the ID of the client
   * @param {boolean} connected if the client is currently connected
   * @param {string} ip the ip of the client
   */
  constructor(id, connected = true, ip) {
    this._id = id;
    this._connected = connected;
    this._ip = ip;
  }

  /**
   * Get the id of the client
   *
   * @returns {string} the id
   */
  get id() {
    return this._id;
  }

  /**
   * Get the ip of the client
   *
   * @returns {string} the ip
   */
  get ip() {
    return this._ip;
  }

  /**
   * Set the connectivity
   *
   * @param {boolean} connected true if the client is currently connected
   */
  set connected(connected) {
    this._connected = connected;
  }

  /**
   * If the client is connected
   *
   * @returns {boolean} true if the client is currenlty connected
   */
  isConnected() {
    return this._connected;
  }
}
