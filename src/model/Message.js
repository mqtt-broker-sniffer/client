import { DateTime } from "luxon";

/**
 * Represents a MQTT message
 */
export class Message {
  /**
   * Build a MQTT message
   *
   * @param {{
   *  topic: string,
   *  sender: string,
   *  message: string,
   *  sn: number,
   *  qos: number,
   *  receivers: [string],
   *  time: number
   * }} json the json of the message
   */
  constructor(json) {
    this.topic_ = json.topic;
    this.sender_ = json.sender;
    this.message_ = Buffer.from(json.message, "utf-8");
    this.sn_ = json.sn;
    this.receivers_ = json.receivers;
    this.qos_ = json.qos;
    this.time_ = DateTime.fromMillis(json.time);
  }

  /**
   * Get the time of the publication of this message
   *
   * @returns {DateTime} the time
   */
  get time() {
    return this.time_;
  }

  /**
   * Get the sender of the message
   *
   * @returns {string} the sender id
   */
  get sender() {
    return this.sender_;
  }

  /**
   * Get the content of the message
   *
   * @returns {Buffer} the content of the message
   */
  get content() {
    return this.message_;
  }

  /**
   * Get the list of receivers
   *
   * @return {[string]} the list of ids of receivers who received this message for now
   */
  get receivers() {
    return this.receivers_;
  }

  /**
   * Get the serial number of the message
   *
   * @returns {number} the serial number
   */
  get serialNumber() {
    return this.sn_;
  }

  /**
   * Get the QOS of a MQTT message
   *
   * @returns {0|1|2} the QOS code
   */
  get qos() {
    return this.qos_;
  }

  /**
   * Get the topic where the emssage was published
   *
   * @return {string} the topic
   */
  get topic() {
    return this.topic_;
  }

  /**
   * Compare 2 messages of a same MQTT broker
   *
   * @param {Message} message the message to compare to
   *
   * @returns {boolean} true if the messages are the same
   */
  equals(message) {
    return this.serialNumber === message.serialNumber;
  }
}
