/**
 * A filter to filter the messages
 */
export default class Filter {
  /**
   * Create a filter
   *
   * @param {boolean} accept if the filter should accept a message per default
   */
  constructor(accept = true) {
    this.check = this.check.bind(this);
    this._default = accept;
  }

  /**
   * Check if the message agrees with the filter
   *
   * @param {import("../Message").Message} message the message to ckeck
   *
   * @returns {boolean} true if the message is accepted by the filter
   */
  check(message) {
    return this._default;
  }

  /**
   * Get the list of messages validated bu the filter
   *
   * @param {[import("../Message").Message]} messages the list of messages
   *
   * @return {[import("../Message").Message]} the list of messages but filtered
   */
  filter(messages) {
    return messages.filter(this.check);
  }
}
