import Filter from "./Filter";

/**
 * A class to make an or filter between other filter
 */
class OrFilter extends Filter {
  /**
   * Create the filter
   *
   * @param {Filter} filter1 the first filter
   * @param {Filter} filter2 the second filter
   */
  constructor(filter1, filter2) {
    super();
    this.check = this.check.bind(this);
    this.filter1 = filter1;
    this.filter2 = filter2;
  }

  /**
   * Check if the message agrees with one of the filter
   *
   * @param {import("../Message").Message} message the message to ckeck
   *
   * @returns {boolean} true if one of the filter accept the message
   */
  check(message) {
    return this.filter1.check(message) || this.filter2.check(message);
  }
}

/**
 * A class to make an and filter between other filter
 */
class AndFilter extends Filter {
  /**
   * Create the filter
   *
   * @param {Filter} filter1 the first filter
   * @param {Filter} filter2 the second filter
   */
  constructor(filter1, filter2) {
    super();
    this.check = this.check.bind(this);
    this.filter1 = filter1;
    this.filter2 = filter2;
  }

  /**
   * Check if the message agrees with both filters
   *
   * @param {import("../Message").Message} message the message to ckeck
   *
   * @returns {boolean} true if both filters accept the message
   */
  check(message) {
    return this.filter1.check(message) && this.filter2.check(message);
  }
}

/**
 * A class to make a not filter
 */
class NotFilter extends Filter {
  /**
   * Create the filter
   *
   * @param {Filter} filter the first filter
   */
  constructor(filter) {
    super();
    this.check = this.check.bind(this);
    this.filter = filter;
  }

  /**
   * Check if the message does not agree with the child filter
   *
   * @param {import("../Message").Message} message the message to ckeck
   *
   * @returns {boolean} true if the child filter does not accept the message
   */
  check(message) {
    return !this.filter.check(message);
  }
}

export { OrFilter, AndFilter, NotFilter };
