import Filter from "./Filter";

/**
 * A class to filter messages by sender
 */
class SenderFilter extends Filter {
  /**
   * Create the filter
   *
   * @param {string} sender the id of the sender
   */
  constructor(sender) {
    super();
    this.check = this.check.bind(this);
    this.sender_ = sender;
  }

  /**
   * Check if the message agrees with the filter
   *
   * @param {import("../Message").Message} message the message to ckeck
   *
   * @returns {boolean} true if the message has the good sender
   */
  check(message) {
    return message.sender === this.sender_;
  }
}

/**
 * A class to filter messages by receiver
 */
class ReceiverFilter extends Filter {
  /**
   * Create the filter, will select the messages where one the receivers is the one passed in argument
   *
   * @param {string} receiver the id of the receiver
   */
  constructor(receiver) {
    super();
    this.check = this.check.bind(this);
    this.receiver_ = receiver;
  }

  /**
   * Check if the message agrees with the filter
   *
   * @param {import("../Message").Message} message the message to ckeck
   *
   * @returns {boolean} true if the message has the good receiver
   */
  check(message) {
    return message.receivers.includes(this.receiver_);
  }
}

/**
 * A class to filter messages by topic
 */
class TopicFilter extends Filter {
  /**
   * Create the filter
   *
   * @param {string} topic the topic
   */
  constructor(topic) {
    super();
    this.check = this.check.bind(this);
    this.topic_ = new RegExp(
      `^(${topic
        .replace(/\/#/, "(/.*)?")
        .replace(/\+/g, "[^/]+")
        .replace(/#/, ".+")})$`
    );
  }

  /**
   * Check if the message agrees with the filter
   *
   * @param {import("../Message").Message} message the message to ckeck
   *
   * @returns {boolean} true if the message has the good topic
   */
  check(message) {
    return this.topic_.test(message.topic);
  }
}

/**
 * A class to filter messages by QOS
 */
class QosFilter extends Filter {
  /**
   * Create the filter
   *
   * @param {string} qos the qos
   */
  constructor(qos) {
    super();
    this.check = this.check.bind(this);
    this.qos_ = qos;
  }

  /**
   * Check if the message agrees with the filter
   *
   * @param {import("../Message").Message} message the message to ckeck
   *
   * @returns {boolean} true if the message has the qos value
   */
  check(message) {
    return message.qos === this.qos_;
  }
}

export { SenderFilter, ReceiverFilter, QosFilter, TopicFilter };
