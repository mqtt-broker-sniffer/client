import Filter from "./Filter";

/**
 * A filter to filter according to content type of a message
 */
class ContentFilter extends Filter {
  /**
   * Create a content type filter
   *
   * @param {(messageContent: Buffer) => boolean} verifier the verifier to check if the message content belongs to a given type
   */
  constructor(verifier) {
    super();
    this._verifier = verifier;
    this.check = this.check.bind(this);
  }

  /**
   * Check if the message agrees with the filter
   *
   * @param {import("../Message").Message} message the message to ckeck
   *
   * @returns {boolean} true if the message has the good type content
   */
  check(message) {
    return this._verifier(message.content);
  }
}

export default ContentFilter;
