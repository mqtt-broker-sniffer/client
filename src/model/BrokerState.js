import { getPublished, getClient, getTopic } from "../net";
import config from "../config";

/**
 * A class that represents the current state of a broker
 */
export default class BrokerState {
  /**
   * Create an empty broker state
   *
   * @param {string} brokerID the ID of the broker
   */
  constructor(brokerID) {
    this._brokerID = brokerID;
    this._messages = [];
    this._clients = [];
    this._topics = [];
    this._next = 0;
  }

  /**
   * Create a new state of a broker with the given information
   *
   * @param {string} brokerID the ID of the broker
   * @param {[import("./Message").Message]} messages the new set of messages
   * @param {[import("./Client").default]} clients the new set of clients
   * @param {[import("./Topic").default]} topics the new set of topics
   */
  _newState(brokerID, messages, clients, topics) {
    const newState = new BrokerState(brokerID);
    newState._messages = messages;
    newState._clients = clients;
    newState._topics = topics;
    newState._next =
      this._messages.length > 0
        ? this._messages[this._messages.length - 1].serialNumber + 1
        : 0;

    return newState;
  }

  /**
   * Create the new state of the broker at the current moment
   *
   * @returns {Promise<BrokerState | 1 | 2>} 1 if the brokerID does not exist, 2 if there is a network error,
   *                                the new broker state otherwise
   */
  async update() {
    const newMessages =
      this._next === 0
        ? await getPublished(this._brokerID, 0, config.maxFetchPublication)
        : await getPublished(this._brokerID, this._next);
    const newClients = await getClient(this._brokerID);
    const newTopics = await getTopic(this._brokerID);

    if (newMessages === 404 || newClients === 404 || newTopics === 404) {
      return 1;
    } else if (
      typeof newMessages === "number" ||
      typeof newClients === "number" ||
      typeof newTopics === "number"
    ) {
      return 2;
    } else {
      const nextIndex = this._messages.findIndex(
        (message) => message.serialNumber === this._next
      );
      return this._newState(
        this._brokerID,
        this._messages
          .slice(
            0,
            this._next === 0 ? 0 : nextIndex === -1 ? undefined : nextIndex
          )
          .concat(newMessages),
        newClients,
        newTopics
      );
    }
  }

  /**
   * Get the set of messages in this state
   *
   * @returns {[import("./Message").Message]} the messages
   */
  getMessages() {
    return this._messages;
  }

  /**
   * Get the set of topics in this state
   *
   * @returns {[import("./Topic").default]} the topics
   */
  getTopics() {
    return this._topics;
  }

  /**
   * Get the set of clients in this state
   *
   * @returns {[import("./Client").default]} the clients
   */
  getClients() {
    return this._clients;
  }
}
