import React from "react";
import ReactJson from "react-json-view";

/**
 * Display the content of the MQTT message as a JSON
 *
 * @param {{content: Buffer}} props the content of the MQTT message
 */
const JSONViewer = ({ content }) => {
  return (
    <ReactJson
      src={JSON.parse(content.toString())}
      name={false}
      theme="harmonic"
      style={{ padding: "5px", borderRadius: "10px" }}
    />
  );
};

export default JSONViewer;
