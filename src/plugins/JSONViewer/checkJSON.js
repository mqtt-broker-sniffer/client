/**
 * Check if the content of a message is a JSON
 *
 * @param {Buffer} content the content of the MQTT message
 *
 * @returns {boolean} true if this is a JSON
 */
export default function (content) {
  try {
    const json = JSON.parse(content.toString("utf-8"));
    return typeof json !== "number" && typeof json !== "string";
  } catch (_) {
    return false;
  }
}
