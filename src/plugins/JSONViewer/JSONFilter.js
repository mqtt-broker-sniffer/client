import React from "react";
import ReactFilterBox from "react-filter-box";
import { SimpleResultProcessing } from "react-filter-box";
import Filter from "../../model/filter/Filter";
import "react-filter-box/lib/react-filter-box.css";

/**
 * A filter for JSON
 */
class JSONFilter extends Filter {
  /**
   *
   * @param expression
   */
  constructor(expression) {
    super();
    this.check = this.check.bind(this);
    this._expression = expression;
  }

  /**
   * Check if the message agrees with the filter
   *
   * @param {import("../../model/Message").Message} message the message to ckeck
   *
   * @returns {boolean} true if the message has the good type content
   */
  check(message) {
    return (
      new SimpleResultProcessing().process(
        [JSON.parse(message.content.toString())],
        this._expression
      ).length === 1
    );
  }
}

/**
 * The JSON filter
 *
 * @param {{onChange: (newFilter: Filter) => void}} props the callback when a new filter is created
 */
const JSONFilterViewer = ({ onChange }) => {
  return <ReactFilterBox onParseOk={(exp) => onChange(new JSONFilter(exp))} />;
};

export default JSONFilterViewer;
