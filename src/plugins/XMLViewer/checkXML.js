/**
 * Check if the content of a message is an XML
 *
 * @param {Buffer} content the content of the MQTT message
 *
 * @returns {boolean} true if this is an XML
 */
export default function (content) {
  const parser = new DOMParser();
  return !parser
    .parseFromString(content.toString("utf-8"), "text/xml")
    .querySelector("parsererror");
}
