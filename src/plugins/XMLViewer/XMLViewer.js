import React from "react";
import XMLViewer from "react-xml-viewer";

/**
 * Display the content of the MQTT message as an XML
 *
 * @param {{content: Buffer}} props the content of the MQTT message
 */
const xmlViewer = ({ content }) => {
  return <XMLViewer xml={content.toString("utf-8")} collapsible={true} />;
};

export default xmlViewer;
