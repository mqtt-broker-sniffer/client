import plugins from "./plugins.json";

/**
 * Load the plugin specification of all the plugins
 *
 * @returns {[Promise<{name: string, path: string, spec: *}>]} list of json import specificing the plugin package with the path of the plugin and the name
 */
function loadPluginPackages() {
  return Object.entries(plugins).map(async ([name, path]) => ({
    name,
    path,
    spec: await import(`./${path}/package.json`),
  }));
}

/**
 * Fetch and import all plugins with the entered feature
 *
 * @param {string} feature the feature that the plugins should support
 *
 * @returns {Promise<[Promise<{name: string, import: *}>]>} the imported features with the name of their package
 */
export default async function fetchPlugins(feature) {
  const plugin = await Promise.all(loadPluginPackages());

  return plugin
    .filter((plugin) => {
      return Object.keys(plugin.spec["features"]).includes(feature);
    })
    .map(async (plugin) => {
      const { name, path, spec } = plugin;

      return {
        name,
        import: (await import(`./${path}/${spec["features"][feature]}`))
          .default,
      };
    });
}
