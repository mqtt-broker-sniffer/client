import { Search } from "@mui/icons-material";
import { Container } from "@mui/material";
import { Toolbar } from "@mui/material";
import { IconButton } from "@mui/material";
import { TextField } from "@mui/material";
import { InputAdornment } from "@mui/material";
import { AppBar } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import logo from "../images/Big_logo.png";

const useStyles = makeStyles(() => ({
  topBar: {
    zIndex: 200,
  },
}));

export default function TopBar() {
  const classes = useStyles();
  const [brokerID, setBrokerId] = useState("");

  return (
    <AppBar className={classes.topBar} position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Link to="/">
            <IconButton size="large">
              <img src={logo} width="100px" alt="" />
            </IconButton>
          </Link>
          <TextField
            variant="outlined"
            label="Broker ID"
            color="secondary"
            focused
            onChange={(e) => setBrokerId(e.target.value)}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Link to={`/published/${brokerID}`}>
                    <IconButton color="secondary">
                      <Search />
                    </IconButton>
                  </Link>
                </InputAdornment>
              ),
            }}
          />
        </Toolbar>
      </Container>
    </AppBar>
  );
}
