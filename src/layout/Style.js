import { createStyles } from "@mui/material";
import { makeStyles } from "@mui/styles";

const Style = makeStyles(() =>
  createStyles({
    "@global": {
      body: {
        margin: 0,
        padding: 0,
        backgroundColor: "#8D9595",
      },
    },
  })
);

export default Style;
