import { makeStyles } from "@mui/styles";
import { Container } from "@mui/material";
import React from "react";
import TopBar from "./TopBar";

const useStyles = makeStyles((theme) => ({
  content: {
    backgroundColor: "#C3C9C9",
    boxShadow: "0px 0px 40px -1px rgb(0 0 0 / 20%)",
    zIndex: -1,
  },
}));

export default function Layout({ children }) {
  const classes = useStyles();
  return (
    <div>
      <TopBar />
      <Container className={classes.content} maxWidth="lg">
        {children}
      </Container>
    </div>
  );
}
