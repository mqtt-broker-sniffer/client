import config from "../config";

const address = config.brokerAddress;
const port = config.brokerAPIPort;

/**
 * Make a HTML request
 *
 * @param {string} path the path of the request
 * @param {*} body the body of the html request
 * @param {string} method 'POST', 'DELETE', 'GET', 'PUT', the method type
 */
async function request(path, body = undefined, method = "POST") {
  const payload = { headers: {} };

  if (body) {
    payload.method = method;
    payload.headers["Content-Type"] = "application/json;charset=UTF-8";
    payload.body = body;
  }
  return await fetch(`http://${address}:${port}${path}`, payload);
}

/**
 * Make an HTML GET request
 *
 * @param {string} path the path of the request
 *
 * @returns Response if the server responds;
 *          -1 otherwise
 */
async function get(path) {
  try {
    const response = await request(path);
    return response;
  } catch (error) {
    return -1;
  }
}

/**
 * Fetch the JSON of the HTML response
 *
 * @param {Response} response the response of an HTML request
 *
 * @returns json if the request is successful;
 *          an HTML error code if the server sent an error code;
 *          -1 if the response is -1
 */
async function getJSONresponse(response) {
  if (response === -1) {
    return -1;
  } else if (!response.ok) {
    return response.status;
  }
  return await response.json();
}

/**
 * Get the JSON of a given path
 *
 * @param {string} path the path of the request
 *
 * @return json if the request is successful;
 *         an HTML error code if the server sent an error code;
 *         -1 if an unexpected error occured
 */
export async function getJSON(path) {
  return await getJSONresponse(await get(path));
}

/**
 * Get the text of a given path
 *
 * @param {string} path the path of the request
 *
 * @return string if the request is successful;
 *         an HTML error code if the server sent an error code;
 *         -1 if an unexpected error occured
 */
export async function getText(path) {
  const response = await get(path);
  if (response === -1) {
    return -1;
  } else if (!response.ok) {
    return response.status;
  }
  return await response.text();
}

/**
 * Make an HTML POST request
 *
 * @param {string} path the path of the request
 * @param {*} body the body of the html request
 *
 * @returns Response if the server responds;
 *          -1 otherwise
 */
async function post(path, body) {
  try {
    const response = await request(path, body);
    return response;
  } catch (error) {
    return -1;
  }
}

/**
 * Post an JSON object in a HTML request
 *
 * @param {string} path the path of the request
 * @param {json} json the object to post
 *
 * @returns the status code of the answer of the servers responds;
 *          -1 otherwise
 */
export async function postJSONgetStatus(path, json) {
  const response = await post(path, JSON.stringify(json));
  if (response === -1) {
    return -1;
  }
  return response.status;
}

/**
 * Post an JSON object in a HTML request and get an JSON in response
 *
 * @param {string} path the path of the request
 * @param {json} json the object to post
 *
 * @return json if the request is successful;
 *         an HTML error code if the server sent an error code;
 *         -1 if an unexpected error occured
 */
export async function postJSONgetJSON(path, json) {
  return await getJSONresponse(await post(path, JSON.stringify(json)));
}

/**
 * Make an HTML PUT request
 *
 * @param {string} path the path of the request
 * @param {*} body the body of the html request
 *
 * @returns Response if the server responds;
 *          -1 otherwise
 */
async function put(path, body) {
  try {
    const response = await request(path, body, "PUT");
    return response;
  } catch (error) {
    return -1;
  }
}

/**
 * Put an JSON object in a HTML request
 *
 * @param {string} path the path of the request
 * @param {json} json the object to update
 *
 * @returns the status code of the answer of the servers responds;
 *          -1 otherwise
 */
export async function putJSONgetStatus(path, json) {
  const response = await put(path, JSON.stringify(json));
  if (response === -1) {
    return -1;
  }
  return response.status;
}

/**
 * Make an HTML PATCH request
 *
 * @param {string} path the path of the request
 * @param {*} body the body of the html request
 *
 * @returns Response if the server responds;
 *          -1 otherwise
 */
async function patch(path, body) {
  try {
    const response = await request(path, body, "PATCH");
    return response;
  } catch (error) {
    return -1;
  }
}

/**
 * Patch an JSON object in a HTML request
 *
 * @param {string} path the path of the request
 * @param {json} json the object to update
 *
 * @returns the status code of the answer of the servers responds;
 *          -1 otherwise
 */
export async function patchJSONgetStatus(path, json) {
  const response = await patch(path, JSON.stringify(json));
  if (response === -1) {
    return -1;
  }
  return response.status;
}
