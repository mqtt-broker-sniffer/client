import Client from "../model/Client";
import { Message } from "../model/Message";
import Topic from "../model/Topic";
import { getJSON, getText } from "./request";

/**
 * Get the list of published messages on a given broker
 *
 * @param {string} brokerID the id of the broker (the username used)
 * @param {number} from the serial number from which we fetch
 * @param {number} last the number that should be return from the last packet submitted. If set, ingore parameter from
 *
 * @returns {Promise<[Message] | number>} the list of messages if success, -1 or a HTML code otherwise
 */
export async function getPublished(brokerID, from = 0, last = undefined) {
  let result = await getJSON(
    `/published/${brokerID}?${
      last === undefined ? `from=${from}` : `last=${last}`
    }`
  );
  if (typeof result === "number") {
    return result;
  }
  return result.map((message) => new Message(message));
}

/**
 * Create a new broker and get the username that we have to use for this broker
 *
 * @returns {Promise<string | number>} the MQTT username to use of success, -1 or a HTML code otherwise
 */
export async function createBroker() {
  return await getText("/new_broker");
}

/**
 * Get the clients of the broker
 *
 * @param {string} brokerID the id of the broker
 *
 * @returns {Promise<[Client] | number>} the list of clients if success, -1 or a HTML code otherwise
 */
export async function getClient(brokerID) {
  let result = await getJSON(`/clients/${brokerID}`);
  if (typeof result === "number") {
    return result;
  }
  return result.map(
    (client) => new Client(client.client, client.connected, client.ip)
  );
}

/**
 * Get the topics of the broker
 *
 * @param {string} brokerID the id of the broker
 *
 * @returns {Promise<[Topic] | number>} the list of topics if success, -1 or a HTML code otherwise
 */
export async function getTopic(brokerID) {
  let result = await getJSON(`/topics/${brokerID}`);
  if (typeof result === "number") {
    return result;
  }
  return result.map((topic) => new Topic(topic.topic, topic.subscribed));
}
