export default {
  brokerAddress: "localhost",
  brokerAPIPort: 4040,
  maxFetchPublication: 100,
};
