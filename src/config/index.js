import base from "./default";
import dev from "./dev";
import production from "./production";

export default {
  ...base,
  ...(process.env.NODE_ENV === "production" ? production : dev),
};
