import React, { useState } from "react";
import { Typography } from "@mui/material";
import { createBroker } from "./net";
import { Link } from "react-router-dom";
import { Container } from "@mui/material";
import { Divider } from "@mui/material";
import { IconButton } from "@mui/material";
import { AddCircle } from "@mui/icons-material";
import config from "./config";
import { Visibility } from "@mui/icons-material";

export default function Home() {
  /**
   * The ID of the broker newly created
   * @type {string}
   */
  const [brokerID, setBrokerID] = useState("");

  /**
   * Click when the a broker is created
   */
  const onClickcreateBroker = () => {
    createBroker().then((value) => {
      if (typeof value !== "number") {
        setBrokerID(value);
      }
    });
  };

  return (
    <Container style={{ width: "100%" }}>
      <Typography variant="h2" color="primary" padding={1} align="center">
        MQTT Viewer
      </Typography>
      <Divider />
      <Typography variant="h5" color="primary" padding={1} align="center">
        <b>Step 1:</b> Create a broker <br />
        <IconButton onClick={onClickcreateBroker} size="large">
          <AddCircle color="secondary" fontSize="large" />
        </IconButton>
      </Typography>

      {brokerID !== "" ? (
        <div>
          <Typography variant="h6" color="secondary" align="center">
            {brokerID}
          </Typography>
          <Typography variant="h5" color="primary" padding={2} align="center">
            <b>Step 2:</b> The ID of the broker is {brokerID}. You have to
            insert this ID as username in your MQTT client to connect to the
            MQTT server.
          </Typography>
          <Typography variant="h5" color="primary" padding={2} align="center">
            <b>Step 3:</b> Connect your client to the following MQTT broker:{" "}
            {config.brokerAdress}
          </Typography>
          <Typography variant="h5" color="primary" padding={2} align="center">
            <b>Step 4:</b> See your MQTT messages <br />
            <Link to={`/published/${brokerID}`}>
              <IconButton>
                <Visibility fontSize="large" color="secondary" />
              </IconButton>
            </Link>
          </Typography>
        </div>
      ) : (
        <div />
      )}
    </Container>
  );
}
