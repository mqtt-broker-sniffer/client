FROM node:16.15.0
WORKDIR /viewer
COPY . .
RUN npm install --force
RUN npm install -g serve
RUN npm run build
CMD serve -s build -l 4030
