const AdmZip = require("adm-zip");
const { exec } = require("child_process");
const fs = require("fs");
const fetch = require("node-fetch");

const args = process.argv.splice(2);
const pluginURL = args[0];

try {
  // Fetch the plugin
  fetch(pluginURL).then(async (res) => {
    if (res.status !== 200) {
      console.log("File not found");
      return;
    }
    // Unzip the plugin
    const zip = new AdmZip(Buffer.from(await res.arrayBuffer()));
    const entries = zip.getEntries();
    const spec = entries.find((entry) => entry.entryName === "package.json");

    // Check the specs of the plugin
    if (spec === undefined) {
      console.log("package.json not found in plugin");
      return;
    }
    const pack = JSON.parse(spec.getData().toString());
    const packageName = pack.name;

    // Install the dependencies
    const dependencies = pack.dependencies;
    Object.entries(dependencies).forEach(([depName, depVersion]) => {
      exec(
        `npm install --save --package-lock-only --no-package-lock ${depName}@${depVersion}`,
        (e, out, err) => {
          console.log(out, err);
        }
      );
    });

    // Add to the list of plugins
    let plugins = JSON.parse(
      fs.readFileSync("./src/plugins/plugins.json").toString()
    );
    plugins[packageName] = packageName;
    fs.writeFileSync("./src/plugins/plugins.json", JSON.stringify(plugins));

    // Write the plugin on the disk
    fs.mkdirSync(`./src/plugins/${packageName}`);
    zip.extractAllTo(`./src/plugins/${packageName}/`);
  });
} catch (exception) {
  console.log(exception);
}
