# net

Fetch messages, topics, and clients from the broker

## Usage

```Javascript
import { getPublished, getClient, getTopic, createBroker } from 'net';
```

---

## getPublished(brokerID, from, last)

Get the list of published messages on a given broker

### Arguments

| Field    | Type     | Default     | Description                                                                                    |
| -------- | -------- | ----------- | ---------------------------------------------------------------------------------------------- |
| brokerID | `string` |             | the id of the broker (the username used)                                                       |
| from     | `number` | `0`         | the serial number from which we fetch                                                          |
| last     | `number` | `undefined` | the number that should be return from the last packet submitted. If set, ingore parameter from |

### Returns

Promise<[<a href="../model/Message.md">Message</a>] | `number`>: the list of messages if success, -1 or a HTML code otherwise

---

## createBroker()

Create a new broker and get the username that we have to use for this broker

### Returns

`Promise<string | number>`: the MQTT username to use of success, -1 or a HTML code otherwise

---

## getClient(brokerID)

Get the clients of the broker

### Arguments

| Field    | Type     | Default | Description          |
| -------- | -------- | ------- | -------------------- |
| brokerID | `string` |         | the id of the broker |

### Returns

Promise<[<a href="../model/Client.md">Client</a>] | `number`>: the list of clients if success, -1 or a HTML code otherwise

---

## getTopic(brokerID)

Get the topics of the broker

### Arguments

| Field    | Type     | Default | Description          |
| -------- | -------- | ------- | -------------------- |
| brokerID | `string` |         | the id of the broker |

### Returns

Promise<[<a href="../model/Topic.md">Topic</a>] | `number`>: the list of topics if success, -1 or a HTML code otherwise
