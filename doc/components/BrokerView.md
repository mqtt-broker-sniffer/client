<!-- doc-md-start -->

# BrokerView

Display the broker view

## Usage

```Javascript
import BrokerView from 'viewer/brokerView/BrokerView.js';
```

## Properties

| Property | PropType                                          | Required | Default | Description                 |
| -------- | ------------------------------------------------- | -------- | ------- | --------------------------- |
| broker   | <a href="../model/BrokerState.md">BrokerState</a> | yes      |         | The broker state to display |

<!-- doc-md-end -->
