<!-- doc-md-start -->
# Published

Display the list of published messages

## Usage

``` Javascript
import Published from 'viewer/Published.js';
```

## Properties

This component has no properties

<!-- doc-md-end -->