<!-- doc-md-start -->

# Clients

Display the boxs for each MQTT client with the ids

## Usage

```Javascript
import Clients from 'viewer/brokerView/Clients.js';
```

## Properties

| Property | PropType                                          | Required | Default | Description                             |
| -------- | ------------------------------------------------- | -------- | ------- | --------------------------------------- |
| broker   | <a href="../model/BrokerState.md">BrokerState</a> | yes      |         | The state of the broker                 |
| sender   | `bool`                                            |          | `true`  | If the clients to represent are senders |

<!-- doc-md-end -->
