<!-- doc-md-start -->

# MessageContent

Display the content of the message with the appropriate display

## Usage

```Javascript
import MessageContent from 'viewer/packets/message/MessageContent.js';
```

## Properties

| Property | PropType | Required | Default | Description                |
| -------- | -------- | -------- | ------- | -------------------------- |
| content  | `Buffer` | yes      |         | The content of the message |

<!-- doc-md-end -->
