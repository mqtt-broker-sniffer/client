<!-- doc-md-start -->

# Telemetry

Display the telemetry of the number of messages

## Usage

```Javascript
import Telemetry from 'viewer/packets/Telemetry.js';
```

## Properties

| Property | PropType                                          | Required | Default                                       | Description                     |
| -------- | ------------------------------------------------- | -------- | --------------------------------------------- | ------------------------------- |
| broker   | <a href="../model/BrokerState.md">BrokerState</a> | yes      |                                               | The state of the broker         |
| filter   | <a href="../model/Filter.md">Filter</a>           |          | <a href="../model/Filter.md">new Filter()</a> | The filter to select the wanted |

<!-- doc-md-end -->
