<!-- doc-md-start -->

# FilterView

A filter window to create filters for the messages

## Usage

```Javascript
import FilterView from 'viewer/packets/filter/FilterView.js';
```

## Properties

| Property | PropType                                              | Required | Default     | Description                               |
| -------- | ----------------------------------------------------- | -------- | ----------- | ----------------------------------------- |
| onChange | `(`<a href="../model/Filter.md">Filter</a>`) => void` |          | `(_) => {}` | The callback when a new filter is created |
| onClose  | `() => void`                                          |          | `() => {}`  | The callback when the panel is closed     |
| open     | `bool`                                                | yes      |             | If the panel is closed                    |
| broker   | <a href="../model/BrokerState.md">BrokerState</a>     | yes      |             | The broker state                          |

<!-- doc-md-end -->
