<!-- doc-md-start -->

# MessageList

Display a list of MQTT messages

## Usage

```Javascript
import MessageList from 'viewer/packets/MessageList.js';
```

## Properties

| Property | PropType                                          | Required | Default                                       | Description                         |
| -------- | ------------------------------------------------- | -------- | --------------------------------------------- | ----------------------------------- |
| broker   | <a href="../model/BrokerState.md">BrokerState</a> | yes      |                                               | The state of the broker             |
| filter   | <a href="../model/Filter.md">Filter</a>           |          | <a href="../model/Filter.md">new Filter()</a> | The filter to apply on the messages |

<!-- doc-md-end -->
