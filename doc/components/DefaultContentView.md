<!-- doc-md-start -->

# DefaultContentView

The default displayer of the content of a MQTT message

## Usage

```Javascript
import DefaultContentView from 'viewer/packets/message/DefaultContentView.js';
```

## Properties

| Property | PropType | Required | Default | Description                |
| -------- | -------- | -------- | ------- | -------------------------- |
| content  | `Buffer` | yes      |         | The content of the message |

<!-- doc-md-end -->
