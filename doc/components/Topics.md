<!-- doc-md-start -->

# Topics

Display the list of topics

## Usage

```Javascript
import Topics from 'viewer/brokerView/Topics.js';
```

## Properties

| Property | PropType                                          | Required | Default | Description             |
| -------- | ------------------------------------------------- | -------- | ------- | ----------------------- |
| broker   | <a href="../model/BrokerState.md">BrokerState</a> | yes      |         | The state of the broker |

<!-- doc-md-end -->
