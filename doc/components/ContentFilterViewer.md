<!-- doc-md-start -->

# ContentFilterViewer

The viewer if the filter of the content for a plugin filter

## Usage

```Javascript
import ContentFilterViewer from 'viewer/packets/filter/ContentFilterViewer.js';
```

## Properties

| Property | PropType                                                             | Required | Default     | Description                             |
| -------- | -------------------------------------------------------------------- | -------- | ----------- | --------------------------------------- |
| onChange | `(`<a href="../model/Filter.md">Filter</a>`) => void`                |          | `(_) => {}` | The callback when the filter is changed |
| plugin   | `{verifier: (Buffer) => boolean, filter: JSX.Element, name: string}` | yes      |             | The plugin                              |

<!-- doc-md-end -->
