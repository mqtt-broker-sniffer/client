<!-- doc-md-start -->

# MQTTMessage

Display the content of a MQTT message

## Usage

```Javascript
import MQTTMessage from 'viewer/packets/message/MQTTMessage.js';
```

## Properties

| Property | PropType                                  | Required | Default | Description            |
| -------- | ----------------------------------------- | -------- | ------- | ---------------------- |
| message  | <a href="../model/Message.md">Message</a> | yes      |         | The message to display |

<!-- doc-md-end -->
