<!-- doc-md-start -->

# PacketFlow

Display the flow of message on the broker

## Usage

```Javascript
import PacketFlow from 'viewer/brokerView/PacketFlow.js';
```

## Properties

| Property | PropType                                          | Required | Default | Description             |
| -------- | ------------------------------------------------- | -------- | ------- | ----------------------- |
| broker   | <a href="../model/BrokerState.md">BrokerState</a> | yes      |         | The state of the broker |

<!-- doc-md-end -->
