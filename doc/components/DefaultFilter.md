<!-- doc-md-start -->

# DefaultFilter

The default filter creator

## Usage

```Javascript
import DefaultFilter from 'viewer/packets/filter/DefaultFilter.js';
```

## Properties

| Property | PropType                                              | Required | Default     | Description                               |
| -------- | ----------------------------------------------------- | -------- | ----------- | ----------------------------------------- |
| onChange | `(`<a href="../model/Filter.md">Filter</a>`) => void` |          | `(_) => {}` | The callback when a new filter is created |
| broker   | <a href="../model/BrokerState.md">BrokerState</a>     | yes      |             | The list of clients                       |

<!-- doc-md-end -->
