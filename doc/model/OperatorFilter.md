# OrFilter extends <a href="./Filter.md">Filter</a>

A class to make an or filter between other filter

## Usage

```Javascript
import { OrFilter } from 'model/filter/OperatorFilters.js';
```

## new OrFilter(filter1, filter2)

Create the filter

### Arguments

| Field   | Type                             | Default | Description       |
| ------- | -------------------------------- | ------- | ----------------- |
| filter1 | <a href="./Filter.md">Filter</a> |         | the first filter  |
| filter2 | <a href="./Filter.md">Filter</a> |         | the second filter |

# AndFilter extends <a href="./Filter.md">Filter</a>

A class to make an and filter between other filter

## Usage

```Javascript
import { AndFilter } from 'model/filter/OperatorFilters.js';
```

## new AndFilter(filter1, filter2)

Create the filter

### Arguments

| Field   | Type                             | Default | Description       |
| ------- | -------------------------------- | ------- | ----------------- |
| filter1 | <a href="./Filter.md">Filter</a> |         | the first filter  |
| filter2 | <a href="./Filter.md">Filter</a> |         | the second filter |

# NotFilter extends <a href="./Filter.md">Filter</a>

A class to make a not filter

## Usage

```Javascript
import { NotFilter } from 'model/filter/OperatorFilters.js';
```

## new NotFilter(filter)

Create the filter

### Arguments

| Field  | Type                             | Default | Description      |
| ------ | -------------------------------- | ------- | ---------------- |
| filter | <a href="./Filter.md">Filter</a> |         | the first filter |
