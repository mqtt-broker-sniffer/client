# Topic

A MQTT topic

## Usage

```Javascript
import Topic from 'model/Topic.js';
```

# Methods

## new Topic(name, subscribed)

Create a topic

### Arguments

| Field      | Type       | Default | Description                    |
| ---------- | ---------- | ------- | ------------------------------ |
| name       | `string`   |         | The name of the topic          |
| subscribed | `[string]` |         | The list of subscribed clients |

---

## get topic()

Get the topic name

### Returns

`string`: The name

---

## get subscribedClients()

Return the list of subsribed clients to the topic

### Returns

`[string]`: The ids of subscribed clients
