# Filter

A filter to filter the messages

## Usage

```Javascript
import Filter from 'model/filter/Filter.js';
```

# Methods

## new Filter(accept)

Create a filter

### Arguments

| Field  | Type      | Default | Description                                       |
| ------ | --------- | ------- | ------------------------------------------------- |
| accept | `boolean` | `true`  | if the filter should accept a message per default |

---

## check(message)

Check if the message agrees with the filter

### Arguments

| Field   | Type                               | Default | Description          |
| ------- | ---------------------------------- | ------- | -------------------- |
| message | <a href="./Message.md">Message</a> |         | the message to ckeck |

### Returns

`boolean`: true if the message is accepted by the filter

---

## filter(messages)

Get the list of messages validated bu the filter

### Arguments

| Field    | Type                                 | Default | Description          |
| -------- | ------------------------------------ | ------- | -------------------- |
| messages | [<a href="./Message.md">Message</a>] |         | the list of messages |

### Returns

[<a href="./Message.md">Message</a>]: the list of messages but filtered
