# Client

A MQTT client

## Usage

```Javascript
import Client from 'model/Client.js';
```

# Methods

## new Client(id, connected, ip)

Build a MQTT client

### Arguments

| Field     | Type      | Default | Description                          |
| --------- | --------- | ------- | ------------------------------------ |
| id        | `string`  |         | The ID of the client                 |
| connected | `boolean` | `true`  | If the client is currently connected |
| ip        | `string`  |         | The ip of the client                 |

---

## get id()

Get the id of the client

### Returns

`string`: The id

---

## get ip()

Get the ip of the client

### Returns

`string`: The ip

---

## set connected(connected)

Set the connectivity

### Arguments

| Field     | Type      | Default | Description                               |
| --------- | --------- | ------- | ----------------------------------------- |
| connected | `boolean` |         | True if the client is currently connected |

---

## isConnected()

If the client is connected

### Returns

`boolean`: True if the client is currently connected
