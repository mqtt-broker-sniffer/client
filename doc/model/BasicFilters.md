# SenderFilter extends <a href="./Filter.md">Filter</a>

A class to filter messages by sender

## Usage

```Javascript
import { SenderFilter } from 'model/filter/BasicFilters.js';
```

## new SenderFilter(sender)

Create the filter

### Arguments

| Field  | Type     | Default | Description          |
| ------ | -------- | ------- | -------------------- |
| sender | `string` |         | the id of the sender |

# ReceiverFilter extends <a href="./Filter.md">Filter</a>

A class to filter messages by receiver

## Usage

```Javascript
import { ReceiverFilter } from 'model/filter/BasicFilters.js';
```

## new ReceiverFilter(receiver)

Create the filter, will select the messages where one the receivers is the one passed in argument

### Arguments

| Field    | Type     | Default | Description            |
| -------- | -------- | ------- | ---------------------- |
| receiver | `string` |         | the id of the receiver |

# TopicFilter extends <a href="./Filter.md">Filter</a>

A class to filter messages by topic

## Usage

```Javascript
import { TopicFilter } from 'model/filter/BasicFilters.js';
```

## new TopicFilter(topic)

Create the filter

### Arguments

| Field | Type     | Default | Description |
| ----- | -------- | ------- | ----------- |
| topic | `string` |         | the topic   |

# QosFilter extends <a href="./Filter.md">Filter</a>

A class to filter messages by topic

## Usage

```Javascript
import { QosFilter } from 'model/filter/BasicFilters.js';
```

## new QosFilter(qos)

Create the filter

### Arguments

| Field | Type     | Default | Description |
| ----- | -------- | ------- | ----------- |
| qos   | `string` |         | the qos     |
