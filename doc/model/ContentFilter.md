# ContentFilter extends <a href="./Filter.md">Filter</a>

A filter to filter according to content type of a message

## Usage

```Javascript
import { ContentFilter } from 'model/filter/ContentFilter.js';
```

## new ContentFilter(verifier)

Create a content type filter

### Arguments

| Field    | Type                  | Default | Description                                                          |
| -------- | --------------------- | ------- | -------------------------------------------------------------------- |
| verifier | `(Buffer) => boolean` |         | the verifier to check if the message content belongs to a given type |
