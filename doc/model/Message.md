# Message

Represents a MQTT message

## Usage

```Javascript
import { Message } from 'model/Message.js';
```

# Methods

## new Message(json)

Build a MQTT message

### Arguments

| Field | Type                                                                                                             | Default | Description             |
| ----- | ---------------------------------------------------------------------------------------------------------------- | ------- | ----------------------- |
| json  | `{ topic: string, sender: string, message: string, sn: number, qos: number, receivers: [string], time: number} ` |         | The json of the message |

---

## get time()

Get the time of the publication of this message

### Returns

`DateTime`: The time

---

## get sender()

Get the sender of the message

### Returns

`string`: The sender id

---

## get content()

Get the content of the message

### Returns

`Buffer`: The content of the message

---

## get receivers()

Get the list of receivers

### Returns

`[string]`: The list of ids of receivers who received this message for now

---

## get serialNumber()

Get the serial number of the message

### Returns

`number`: the serial number

---

## get qos()

Get the QOS of a MQTT message

### Returns

`0|1|2`: the QOS code

---

## get topic()

Get the topic where the emssage was published

### Returns

`string`: the topic

---

## equals(message)

Compare 2 messages of a same MQTT broker

### Arguments

| Field   | Type                              | Default | Description               |
| ------- | --------------------------------- | ------- | ------------------------- |
| message | <a href="./Message.md">Message<a> |         | the message to compare to |

### Returns

`boolean`: true if the messages are the same
