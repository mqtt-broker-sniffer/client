# BrokerState

A class that represents the current state of a broker

## Usage

```Javascript
import BrokerState from 'model/BrokerState.js';
```

# Methods

## new BrokerState(brokerID)

Create an empty broker state

### Arguments

| Field    | Type     | Default | Description          |
| -------- | -------- | ------- | -------------------- |
| brokerID | `string` |         | the ID of the broker |

---

## update()

Create the new state of the broker at the current moment

### Returns

Promise<<a href="BrokerState">BrokerState</a> | `1` | `2`>: 1 if the brokerID does not exist, 2 if there is a network error, the new broker state otherwise

---

## getMessages()

Get the set of messages in this state

### Returns

[<a href="./Message.md">Message</a>]: the messages

---

## getTopics()

Get the set of topics in this state

### Returns

[<a href="./Topic.md">Topic</a>]: the topics

---

## getClients()

Get the set of clients in this state

### Returns

[<a href="./Client.md">Client</a>]: the clients
